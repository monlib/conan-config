# conan-config

This repo contains the canonical configuration and profiles for monlib
packages. 

# License

These configuration files are licensed under the MIT license. Some files are
based off of the conan provided defaults which were created by JFrog LTD and
licensed under the MIT license.
